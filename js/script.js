// Variable declaration
const $mainChat = document.querySelector('.main-chat');
const $users = document.querySelector('.chats');
const $subscriber = document.querySelector('.subscriber input');
const $message = document.querySelector('.elements input')
const $subcribeButton = document.querySelector('.subscriber button');
const $sendButton = document.querySelector('.elements button');

class Observer {
    constructor() {
        this.subscribers = [];
    }

    subscribe(subscriber) {
        this.subscribers.push(subscriber);
    }

    unsubscribe(subscriber) {
        this.subscribers = this.subscribers.filter(sub => sub !== subscriber);
    }

    sendNotify(subscriber) {
        this.subscribers.forEach(sub => {
            if(sub.name !== subscriber){
                sub.messages.call(sub, subscriber);
                sub.notification(sub);
            }
        })
    }
}

class Subscriber {
    constructor(name) {
        this.name = name;
        console.log(`Creating subscriber: ${this.name}`);
    }

    messages(sub) {
        console.log(`${this.name} has a message from ${sub}`);
    }

    notification(sub){
        const $subscriber = document.getElementById(sub.name);
        $subscriber.style = 'background: #27b7fa; color: #fff;'
    }
}

const displayUsersMessage = (sub) => {
    const $contact = document.createElement('div');
    const $icon = document.createElement('div');
    const $names = document.createElement('p');
    $contact.classList.add('contact');
    $icon.classList.add('icon');
    $icon.setAttribute('id', sub)
    $names.innerHTML = sub;
    $icon.innerHTML = sub[0];
    $contact.appendChild($icon);
    $contact.appendChild($names);
    $users.appendChild($contact);

    $contact.addEventListener('click', () => {
        $users.querySelectorAll('.contact').forEach(contact => {
            contact.classList.remove('select');
        });
        $contact.classList.add('select');
        $icon.style = 'background-color: #fff; color: #b8b8b8;'
    });
}

const getAuthor = () => {
    return $users.querySelector('.select p').innerHTML;
}

const sendMessage = () => {
    let $author = getAuthor();
    const $messageBubble = document.createElement('div');
    const $name = document.createElement('h3');
    const $text = document.createElement('p');
    $name.innerHTML = $author;
    $text.innerHTML = $message.value;
    $messageBubble.classList.add('message');
    $messageBubble.appendChild($name);
    $messageBubble.appendChild($text);
    $mainChat.appendChild($messageBubble);
    observer.sendNotify($author);
}

const subscribeUser = () => {
    let $userName = $subscriber.value;
    observer.subscribe(new Subscriber($userName))
    displayUsersMessage($userName);
}

$subcribeButton.addEventListener('click', subscribeUser);
$sendButton.addEventListener('click', sendMessage);

const observer = new Observer();


// const observer = new Observer();
// const daniela = new Subscriber('Daniela');
// const roberto = new Subscriber('Roberto');
// const paola = new Subscriber('Paola');

// observer.subscribe(daniela);
// observer.subscribe(roberto);
// observer.subscribe(paola);

// console.log("----Group Chat----")

// observer.sendNotify(daniela, "Hello!");
// observer.sendNotify(roberto, "Hi!")